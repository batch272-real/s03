-- 1. INSERTING RECORDS IN users TABLE

INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 1");

INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 2");

INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 3");

INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 4");

INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 5");

-- 2. INSERTING RECORDS IN posts TABLE

INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", "2021-01-02 1");

INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth!", "2021-01-02 2");

INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (2, "Third Code", "Welcome to Mars!", "2021-01-02 3");

INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (4, "Fourth Code", "Bye bye solar system!", "2021-01-02 4");

-- 3. RETRIEVING RECORDS WITH user_id = 1 IN posts

SELECT * FROM posts WHERE user_id = 1;


-- 4. RETRIEVING ALL user's email and datetime_created

SELECT email, datetime_created FROM users;

-- 5. UPDATING CONTENT TO "Hello to the people of the Earth" WHERE ITS INITIAL CONTENT IS "Hello Earth!"

UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

-- 4. DELEING user's email = "johndoe@gmail.com"

DELETE FROM users WHERE email = "johndoe@gmail.com";