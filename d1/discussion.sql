[SECTION] -- INSERTING RECORDS


-- inserting data to a particular table and/width 1 column

INSERT INTO artists (name) VALUES ("Rivermaya");

INSERT INTO artists (name) VALUES ("Psy");


-- inserting data to a particular table and/width 2 or more columns

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy 6", "2012-1-1", 2);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-1-1", 1);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam style", 253, "K-POP", 1);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Ulan", 234, "OPM", 2);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("214", 256, "OPM", 2);


[SECTION] -- READ and SELECT RECORDS FOR DATA

-- display the title and genre of all the songs

SELECT song_name, genre FROM songs

SELECT song_name FROM songs WHERE genre = "OPM";

-- display all values in songa

SELECT * FROM songs;

-- display the title and length of the OPM songs that are more than 2 minutes

SELECT song_name, length FROM songs WHERE genre = "OPM" AND length > 201;


[SECTION] -- UPDATING RECORDS

UPDATE songs SET length = 259 WHERE song_name = "214";

UPDATE songs SET song_name = "Ulan updated" WHERE song_name = "Ulan";


[SECTION] -- DELETE RECORD

DELETE FROM songs WHERE genre = "OPM" AND length > 250;